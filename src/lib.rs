extern crate reqwest;
#[macro_use]
extern crate log;

pub mod alert {
    use std::fmt;
    pub struct AlertError;
    impl fmt::Display for AlertError {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "Alert sending Error") // user-facing output
        }
    }

    // Implement std::fmt::Debug for AppError
    impl fmt::Debug for AlertError {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "{{ file: {}, line: {} }}", file!(), line!()) // programmer-facing output
        }
    }

    pub trait Alerter {
        fn send_message(&self, msg: &str) -> Result<(), AlertError>;
    }

    pub mod dummy {
        use super::{AlertError, Alerter};
        pub struct DummyAlert;

        impl Alerter for DummyAlert {
            fn send_message(&self, _msg: &str) -> Result<(), AlertError> {
                Ok(())
            }
        }
    }
    pub mod telegram {
        use super::{AlertError, Alerter};
        use std::collections::HashMap;
        use reqwest::blocking::Client;
        pub static TELEGRAM_API_ENDPOINT: &str = "https://api.telegram.org/";
        pub struct TelegramAlert {
            api_endpoing: String,
            bot_token_api: String,
            chat_id: String,
        }
        impl TelegramAlert {
            // initialize
            pub fn new(api_endpoing: &str, bot_token_api: &str, chat_id: &str) -> TelegramAlert {
                TelegramAlert {
                    api_endpoing: api_endpoing.to_string(),
                    bot_token_api: bot_token_api.to_string(),
                    chat_id: chat_id.to_string(),
                }
            }
        }
        impl Alerter for TelegramAlert {
            fn send_message(&self, msg: &str) -> Result<(), AlertError> {
                let mut json: HashMap<String, String> = HashMap::new();
                json.insert("chat_id".to_string(), self.chat_id.to_string());
                json.insert("parse_mode".to_string(), "HTML".to_string());
                json.insert("text".to_string(), msg.to_string());
                let url = format!(
                    "{}bot{}/sendMessage",
                    self.api_endpoing, self.bot_token_api
                );
                debug!("send_message(): url: {}", url);
                let client = Client::new();
                let _res = client.post(&url).json(&json).send();
                debug!("{:?}", _res);
                Ok(())
                // match res {
                //     Ok(r) => Ok(()),
                //     Err(e) => {Err}
                // }
            }
        }
    }
}
