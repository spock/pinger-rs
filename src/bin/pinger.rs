use oping::{Ping, PingIter, PingResult};
use std::path::{Path, PathBuf};
use std::{thread, time};
extern crate structopt;
use std::io::{stdout, Write};
use structopt::StructOpt;
//use std::io;
//use std::fs;
use std::fs::OpenOptions;
use std::process::exit;

#[macro_use]
extern crate log;
//use log::Level;

use serde_derive::Deserialize;

use chrono::{DateTime, Utc};
extern crate toml;

use pinger::alert;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "pinger",
    about = "Pinger is app for constantly ping host and report missing IMCP via misc plugins",
    author = "Marcin Jurczuk <marcin@jurczuk.eu>"
)]
struct Opt {
    /// Activate debug mode
    // short and long flags (-d, --debug) will be deduced from the field's name
    #[structopt(short, long)]
    debug: bool,

    #[structopt(short = "c", long = "config", default_value = "/etc/pinger.conf")]
    cfgpath: String,
    // Input file
    //#[structopt(parse(from_os_str))]
    //input: PathBuf,

    // Output file, stdout if not present
    #[structopt(parse(from_os_str), short = "l", long = "logfile")]
    output: Option<PathBuf>,
    // Where to write the output: to `stdout` or `file`
    //#[structopt(short)]
    //out_type: String,

    // File name: only required when `out` is set to `file`
    //#[structopt(name = "FILE", required_if("out_type", "file"))]
    //file_name: Option<String>,
}

// Configuration
#[derive(Deserialize, Debug)]
struct Config {
    host: String,
    telegram_api_token: Option<String>,
    telegram_chat_id: Option<String>,
}

fn do_pings(host: &str) -> PingResult<PingIter> {
    let mut ping = Ping::new();
    ping.set_timeout(5.0)?; // timeout of 5.0 seconds
    ping.add_host(host)?; // fails here if socket can't be created
                          //ping.add_host("212.33.64.2")?;
    ping.send()

    //Ok(())
}

fn get_config_str(path: &Path) -> Result<String, std::io::Error> {
    debug!("get_config_str(): reading {:?}", path.to_str());
    std::fs::read_to_string(path)
}

fn get_alert_provider(config: &Config) -> Box<dyn alert::Alerter> {
    match &config.telegram_api_token {
        Some(id) => {
            if id.len() > 0 {
            debug!(
                "Telegram enabled, TELEGRAM_API_ENDPOINT: {}",
                alert::telegram::TELEGRAM_API_ENDPOINT
            );
            Box::new(alert::telegram::TelegramAlert::new(
                alert::telegram::TELEGRAM_API_ENDPOINT,
                &config.telegram_api_token.as_ref().unwrap(),
                &config.telegram_chat_id.as_ref().unwrap()
            ))
        } else {
            Box::new(alert::dummy::DummyAlert {})
        }
        }
        None => Box::new(alert::dummy::DummyAlert {}),
    }
}
fn main() {
    env_logger::init();
    //Builder::new().filter(None,LevelFilter::Info).init();
    let opts = Opt::from_args();
    info!("Using {} as config.", opts.cfgpath);
    let cfgpath = Path::new(&opts.cfgpath);
    let cfdata = match get_config_str(cfgpath) {
        Ok(c) => c,
        Err(e) => {
            error!("Unable to read config from {}: {}", opts.cfgpath, e);
            exit(-1)
        }
    };
    let config: Config = match toml::from_str(&cfdata) {
        Ok(c) => c,
        Err(e) => {
            warn!("Config file parsing erorr!: {}", e);
            exit(-1)
        }
    };
    debug!("Config: {:?}", config);
    //info!("Starting ICMP probe to {}");
    let mut output = match opts.output {
        Some(p) => match OpenOptions::new()
            .write(true)
            .create(true)
            .append(true)
            .open(p.as_path())
        {
            Ok(f) => Box::new(f) as Box<dyn Write>,
            Err(e) => {
                warn!("Error opening logfile {}", e);
                Box::new(stdout())
            }
        },
        None => Box::new(stdout()) as Box<dyn Write>,
    };
    let lulu = time::Duration::from_secs(1);
    let mut icmp_total = 0;
    let mut icmp_tmp_ok = 0;
    let mut icmp_tmp_ng = 0;
    let alerter = get_alert_provider(&config);
    alerter.send_message(&format!("Starting pinger for host {}", &config.host));
    loop {
        debug!(
            "Counters: TOTAL: {}, OK: {}, NO:{}",
            icmp_total, icmp_tmp_ok, icmp_tmp_ng
        );
        if icmp_total % 60 == 0 {
            info!(
                "60 sec summary: TOTAL: {} OK: {} NG:{}",
                icmp_total, icmp_tmp_ok, icmp_tmp_ng
            );
            icmp_tmp_ng = 0;
            icmp_tmp_ok = 0;
        }
        icmp_total += 1;
        match do_pings(&config.host) {
            Ok(responses) => {
                for resp in responses {
                    let now: DateTime<Utc> = Utc::now();
                    if resp.dropped > 0 {
                        println!(
                            "{} No response from host: {}",
                            now.to_rfc3339(),
                            resp.hostname
                        );
                        icmp_tmp_ng += 1;
                    } else {
                        let _ = output.write_all(
                            format!(
                                "{} Response from host {} (address {}): latency {} ms\n",
                                now.to_rfc3339(),
                                resp.hostname,
                                resp.address,
                                resp.latency_ms
                            )
                            .as_bytes(),
                        );
                        let _ = output.flush();
                        icmp_tmp_ok += 1;
                    }
                }
            }
            Err(e) => {
                println!("Error sending ICMP echo request: {}", e);
            }
        }
        // let sleep a moment
        thread::sleep(lulu);
    }
}
